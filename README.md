# fenilgadhiya-simplyloosechatbot

## Name
Chatbot for simplyloose website

## Description
I create a chatbot for filling form of fitness website for better interaction with user and user.

## Visuals
1. STORY WRITING of chatbot : [https://ibb.co/72kYzsQ](url)
2. TABLE WRITING of chatbot : [https://ibb.co/HpNPgdd](url)
3. Working of CHATBOT : [https://ibb.co/SP53z1v](url) 

## Installation 
**BOTFRONT**
Technical Requirements :
 1. Linux or macOS (Windows may work but is not officially supported yet, help is welcome)
 2. Chrome (some issues with other browsers)
 3. A recent version of Docker
 4. A recent version of Node.js

STEPS:
 1. Install the botfront CLI : **_npm install -g botfront_**
 2. Create new project folder : **_botfront init_**
 3. Start botfront : **_botfront up_**

## Usage
Task bot botfront here is to create story which manage the chatbot. how it can converse. we have table which manage the questions asked by bot.

## Support
If one faces problem in botfront then they may visit the website [https://botfront.io/](url).

## Authors and acknowledgment
I , the developer of a “Chatbots for Simplyloose website”, with immense pleasure and commitment would like to present the project assignment. The development of this project has given me wide opportunity to think, implement and interact with various aspects of management skills as well as the new emerging technologies.

Every work that one completes successfully stands on the constant encouragement, good will and support of the people around. I hereby avail this opportunity to express my gratitude to number of people who extended their valuable time, full support and cooperation in developing the project. I express deep sense of gratitude towards our internship guide Mr. RUshikesh Patel(CEO and FOUNDER,kintu designs PVT LTD) for the support during the whole session of study and development. It is because of them, that I was prompted to do hardwork, adopting new technologies.
Kintu designs Email : support@kintudesigns.com
Phone : +91 261 2977123, Mobile : +91 95860 49482

